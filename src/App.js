import React, { Component } from "react";
import Main from "./layout/Main";

class App extends Component {
  render() {
    return (
      <Main />
    );
  }
}

export default App;
