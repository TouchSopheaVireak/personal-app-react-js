import React, { Component } from "react";
import {Card, Col, Row, Container, Table, td} from 'react-bootstrap';
import {
        BrowserRouter as Router,
        Switch,
        Route,
        Link
       } from "react-router-dom";
import * as randomId from 'random-id';
import '../Style/style.css';
/* Import Components */
import Input from "../components/Input";
import Button from "../components/Button";
import CheckBox from "../components/CheckBox";
import CardBox from "../components/CardBox";
import Modals from "../components/Modals";
import Tables from "../components/Tables";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      personal_data: {
        id: "",
        name: "",
        age: "",
        gender: "",
        created: 0,
        modified: 0,
        job:[],
        per_data: [],
      },
      fields: {},
      errors: {},
      jobOption: ["Student", "Teacher", "Developer"],
      buttonState:"Submit",
      ModalState:false,
      selectedGender:"",
      modalData:{
        name: "",
        gender: "",
        job:[],
      }
    };

    this.handleName = this.handleName.bind(this);
    this.handleAge = this.handleAge.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
    this.handleCheckBox = this.handleCheckBox.bind(this);
    this.selectRadio = this.selectRadio.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  };

  handleName(e) {
    let value = e.target.value;
    this.setState (
      prevState => ({
        personal_data: {
          ...prevState.personal_data,
          name: value
        }
      }),
    );
  };

  handleAge(e) {
    let value = e.target.value;
    this.setState (
      prevState => ({
        personal_data: {
          ...prevState.personal_data,
          age: value
        }
      }),
    );
  };

  selectRadio(e) {
    let value = e.target.value;
    this.setState({
      selectedGender: value
    });
    this.setState (
      prevState => ({
        personal_data: {
          ...prevState.personal_data,
          gender: value
        }
      }),
    );
  };

  handleCheckBox(e) {
    const newSelection = e.target.value;
    let newSelectionArray;

    if (this.state.personal_data.job.indexOf(newSelection) > -1) {
      newSelectionArray = this.state.personal_data.job.filter(
        s => s !== newSelection
      );
    } else {
      newSelectionArray = [...this.state.personal_data.job, newSelection];
    }

    this.setState(prevState => ({
      personal_data: { ...prevState.personal_data, job: newSelectionArray }
    }));
  };

  handleFormSubmit(e) {
    e.preventDefault();
    let userData = this.state.personal_data;
    this.state.personal_data.id = randomId(5, 'aA0');
    this.setState(
      prevState => ({
        personal_data: { ...prevState.personal_data, 
                per_data: this.state.personal_data.per_data.concat([userData])}
      }),
    );
  };

  
  handleDelete(data) {
    const items = this.state.personal_data.per_data.filter(item => item.id !== data.id);
    this.setState(
      prevState => ({
        personal_data: { ...prevState.personal_data, 
                per_data: items}
      }),
    );  
  };

  // clear
  handleClearForm() {
    this.setState(
      prevState => ({
        personal_data: { ...prevState.personal_data, 
          id: "",
          name: "",
          age: "",
          gender: "",
          job:[]
        }
      }),
    );
    document.getElementById('maleRadio').checked=false;
    document.getElementById('femaleRadio').checked=false;
  }

  handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    //Name
    if(!fields["name"]){
      formIsValid = false;
      errors["name"] = "Cannot be empty";
    }

    if(typeof fields["name"] !== "undefined"){
      if(!fields["name"].match(/^[a-zA-Z]+$/)){
        formIsValid = false;
        errors["name"] = "Only letters";
      }      	
    }
    
    this.setState({errors: errors});
    return formIsValid;
  }
  handleEdit(e) {
    let id = e.target.value
    let itemToUpdate;
    this.setState(state => {
        const list = state.personal_data.per_data.map((item) => {
          if (item.id === id) {
            itemToUpdate=item;
            if(itemToUpdate.gender==="Male")
              document.getElementById('maleRadio').checked=true;
            else
            document.getElementById('femaleRadio').checked=true;
            return item;
          } else {
            return item;
          }
        });
        return {
          list,
        };
       
    });
    this.setState (
      prevState => ({
        personal_data: {
          ...prevState.personal_data,
          id:itemToUpdate.id,
          name: itemToUpdate.name,
          age:itemToUpdate.age,
          gender:itemToUpdate.gender,
          created:itemToUpdate.created,
          modified:itemToUpdate.modified,
          job:itemToUpdate.job,
        },
      })
    );
    this.setState(
      prevState => ({
        personal_data: { ...prevState.personal_data},
        buttonState:"Update"
      })
    );

  }
  handleFormSubmit(e) {
  if(this.state.personal_data.name !== "" && this.state.personal_data.age !== ""
    && this.state.selectedGender !== "" && this.state.personal_data.job.length != 0
  )
  {
    if(this.state.buttonState === "Submit") {
      e.preventDefault();
      let userData = this.state.personal_data;
      this.state.personal_data.id = randomId(5, 'aA0');
      this.setState(
      prevState => ({
        personal_data: { ...prevState.personal_data, 
                per_data: this.state.personal_data.per_data.concat([userData])}
      }),
      );
    }
    else
    {
      e.preventDefault();
      let userData = this.state.personal_data;
      this.setState(state => {
        const list = state.personal_data.per_data.map((item) => {
          if (item.id === userData.id) {
            item.name=userData.name;
            item.age=userData.age;
            item.gender=userData.gender;
            item.job=userData.job;
            item.created=userData.created;
            return item;
          } else {
            return item;
          }
        });
  
        return {
          list,
        };
      });
      this.setState(
          prevState => ({
            personal_data: { ...prevState.personal_data},
            buttonState:"Submit"
          })
        );
    }
    
    this.handleClearForm();
    }
    else 
      alert("Cannot Submit")
  };

  delete(e) {
    let id = e.target.value
    this.setState(
      prevState => ({
        personal_data: {...prevState.personal_data, 
        per_data: this.state.personal_data.per_data.filter(item=>item.id!=id)}
      }),
    );
  }

  view(e){
    let id=e.target.value
    let itemToShow;
    this.setState(
      prevState => ({
        personal_data: { ...prevState.personal_data},
        ModalState:true
      })
    );
    this.setState(state => {
      const list = state.personal_data.per_data.map((item) => {
        if (item.id === id) {
          itemToShow=item;
          return item;
        } else {
          return item;
        }
      });
      return {
        list,
      };   
    });

    this.setState(
      prevState => ({
        personal_data: { ...prevState.personal_data},
        modalData:itemToShow,
      }),
    );
  }

  modalClose(e){
    this.setState(
      prevState => ({
        personal_data: { ...prevState.personal_data},
        ModalState:false
      })
    );
  }

render() {
    return (
        <Container style = {{marginTop: "5%",  marginBottom: "3%"}}>
          <h1 className = "header">Personal Information</h1>
          <Card>
            <Card.Body>
              <Row>
                <Col sm={"6"}>
                  <form className="container-fluid" onSubmit = {this.handleFormSubmit}>
                    <Input
                      inputType = {"text"}
                      name = {"name"}
                      title = {"Name"}
                      value = {this.state.personal_data.name}
                      placeholder = {"name"}
                      handleChange = {this.handleName}                             
                    />
                    <Input
                      inputType = {"number"}
                      name = {"age"}
                      title = {"Age"}
                      value = {this.state.personal_data.age}
                      placeholder = {"Age"}
                      handleChange = {this.handleAge}
                    />
                    <Button
                      action = {this.handleFormSubmit}
                      type = {"dark"}
                      title = {this.state.buttonState==="Submit"?"Submit":"Update"}
                      clear = {this.handleButtonClick}
                    />
                    
                  </form>   
                </Col>
                <Col sm={"6"}>
                    <Row>
                      <h5>Gender</h5>
                    </Row>

                    <Row>
                      <div className="radio" style={{margin: 5}}>
                        <label style={{marginRight: 3}}>
                          <input
                            id="maleRadio"
                            style={{marginRight: 3}}
                            name="gender"
                            type="radio"
                            value="Male"
                            onChange={this.selectRadio}
                          />
                          Male
                        </label>
                        <input
                            id="femaleRadio"
                            style={{marginRight: 5}}
                            name="gender"
                            type="radio"
                            value="Female"
                            onChange={this.selectRadio}
                          />
                          Female
                      </div>
                    </Row>

                    <Row>
                      <Modals data={this.state.modalData} show={this.state.ModalState} close={this.modalClose.bind(this)}/>
                      <CheckBox
                        title={"Job"}
                        name={"Job"}
                        options={this.state.jobOption}
                        selectedOptions={this.state.personal_data.job}
                        handleChange={this.handleCheckBox}
                      />{" "}
                    </Row>
                  </Col>
                </Row>
              </Card.Body>
            </Card>

            <Router>
              <div className="mt-2 "> 
                <center>
                 <Link to="/list" className="mr-2"><Button title="List"></Button></Link>
                 <Link to="/card"><Button title="Card"></Button></Link>
                  <Switch>
                    <Route path="/list">
                      <Row>
                        <Tables view={this.view.bind(this)} edit={this.handleEdit} delete={this.delete.bind(this)}>{this.state.personal_data.per_data}</Tables>
                      </Row>
                    </Route>
                    <Route path="/card">
                      <Row className="mt-2 ">
                          {this.state.personal_data.per_data.map((value, index) => {
                            return <CardBox view={this.view.bind(this)}
                                            edit={this.handleEdit}
                                            delete={this.delete.bind(this)}
                            key={index}>{value}</CardBox>;
                          })}
                      </Row>
                    </Route>
                  </Switch>
                </center>
              </div>
          </Router>
        </Container>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};

export default Main;
