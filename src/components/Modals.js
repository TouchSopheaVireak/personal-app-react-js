import React from "react";
import {Button,Modal} from 'react-bootstrap';

const Modals = props => {
  
  return (
      <Modal show={props.show}>
        <Modal.Header closeButton>
          <Modal.Title>{props.data.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Gender : {props.data.gender}</h5>
          <h5>Job : </h5>
          <ul>
            {props.data.job.map(item=>{
                return <li>{item}</li>
            })}
          </ul>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={props.close}>
            Close
          </Button>
          <Button variant="primary">
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
  );
};

export default Modals;
