import React from "react";
import { Card, Button,ListGroup,Dropdown,DropdownButton} from "react-bootstrap";
import * as moment from 'moment';
import 'moment/locale/km'
const CardBox = props => {
    let created = moment(props.children.created).locale('kh').fromNow();
    return (
    <Card className="ml-5 mb-3 text-left" style={{ width: '13rem'}} >
      <Card.Header>
      <DropdownButton id="dropdown-item-button" title="Action" style={{ width: '13rem'}} >
        <Dropdown.Item as="button" value={props.children.id} onClick={props.view}>View</Dropdown.Item>
        <Dropdown.Item as="button" value={props.children.id} onClick={props.edit}>Edit</Dropdown.Item>
        <Dropdown.Item as="button" value={props.children.id} onClick={props.delete}>Delete</Dropdown.Item>
      </DropdownButton>
      </Card.Header>
      <Card.Body>
        <Card.Title><h3>{props.children.name}</h3></Card.Title>
        <Card.Text>
          <h6>Job :</h6>
          <ul>
            {props.children.job.map(item=>{
                return <li>{item}</li>
            })}
          </ul>
        </Card.Text>
      </Card.Body>
      <Card.Footer className="text-muted">{created}</Card.Footer>
    </Card>
    );
};
export default CardBox;
