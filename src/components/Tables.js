import React, { Component } from "react";
import { Table, Button, Container } from "react-bootstrap";
import Pagination from "react-js-pagination";
import 'bootstrap/dist/css/bootstrap.min.css';
import * as moment from 'moment';
import 'moment/locale/km'
// import {Clock} from 'react-clock'
  export default class Paginations extends Component{
    constructor(props) {
      super(props);
      this.state = {
        activePage: 1
      };
    }
  handlePageChange(pageNumber) {
      console.log(`active page is ${pageNumber}`);
      this.setState({activePage: pageNumber});
  }
  render(){
   let created = moment(this.props.children.created).locale('kh').fromNow();
   let modified = moment(this.props.children.modified).locale('kh').fromNow();
   const indexOfLast =this.state.activePage * 3;
   const indexOfFirst = indexOfLast - 3;
   const current = this.props.children.slice(indexOfFirst, indexOfLast);
  return (
    <Container>
      <Table responsive>
          <thead class="thead-dark">
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Age</th>
                <th>Gender</th>
                <th>Job</th>
                <th>Create At</th>
                <th>Update At</th>
                <th>Action</th>
              </tr>
          </thead>
          <tbody>
              {
              current.map((value, index) => {
                return <tr key={index}>
                  <td>{value.id}</td>
                  <td>{value.name}</td>
                  <td>{value.age}</td>
                  <td>{value.gender}</td>
                  <td>{value.job.map(function(item) {
                    return <li key = {item}>
                      <a href="#">{item}</a>
                    </li>;
                  })}</td>

                  <td>{created}</td>
                  <td>{modified}</td>
                  <td><Button className="mr-1" variant="info" value={value.id} onClick={this.props.view} >View </Button> 
                      <Button className="mr-1" variant="warning" value={value.id} onClick={this.props.edit}>Edit</Button>    
                      <Button variant="danger" value={value.id} onClick = {this.props.delete}>Delete</Button>
                  </td>
                </tr>;
              })}
          </tbody>
      </Table>
        <div style = {{marginLeft: "40%"}}>
          <Pagination 
            prevPageText='prev'
            nextPageText='next'
            activePage={this.state.activePage}
            itemsCountPerPage={3}
            totalItemsCount={this.props.children.length}
            linkClass={"page-link"}
            itemClass={"page-item"}
            onChange={this.handlePageChange.bind(this)}
          />
        </div>
    </Container>
  );
  }
};