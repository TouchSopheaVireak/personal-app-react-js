import React from "react";

const Button = props => {
  return (
    <button
      style = {props.style}
      className = {
        props.type === "dark" ? "btn btn-dark" : "btn btn-outline-dark"
      }
      onClick = {props.action}
      clear = {props.clear}
    >
      {props.title}
    </button>
  );
};

export default Button;
