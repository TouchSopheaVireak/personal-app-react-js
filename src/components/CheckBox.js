import React from "react";

const CheckBox = props => {
  console.log(props);
  return (
    <div className="form-group">
      <h5 for={props.name} className="form-label">
        {props.title}
      </h5>
      <div className="checkbox">
        {props.options.map(option => {
          return (
            <label key={option} className="checkbox-inline">
              <input
                style={{margin: 5}}
                id={props.name}
                name={props.name}
                onChange={props.handleChange}
                value={option}
                checked={props.selectedOptions.indexOf(option) > -1}
                type="checkbox"
              />
              {option}
            </label>
          );
        })}
      </div>
    </div>
  );
};

export default CheckBox;